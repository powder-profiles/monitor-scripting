#!/bin/sh
#
# Install the RF monitor and its dependencies.
#
sudo apt-get update
if [ $? -ne 0 ]; then
    echo 'apt-get update failed'
    exit 1
fi

sudo apt-get -y install --no-install-recommends libxml-libxml-perl libjson-perl
if [ $? -ne 0 ]; then
    echo 'apt-get install libxml-libxml-perl failed'
    exit 1
fi

#
# Install the portal tools code.
#
/local/repository/portal-tools/install.sh
if [ $? -ne 0 ]; then
    echo '/local/repository/portal-tools/install.sh failed'
    exit 1
fi

#
# Install a minimal web server, from the monitor-spectrum repo.
#
/local/repository/monitor-spectrum/install-nginx.sh
if [ $? -ne 0 ]; then
    echo '/local/repository/monitor-spectrum/install-nginx.sh failed'
    exit 1
fi

#
# This replaces listing.php from monitor-spectrum submodule, does subdirs
#
cp -fp /local/repository/listing.php /local/www

exit 0
